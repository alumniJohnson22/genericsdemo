package com.m3.training.genericsdemo;

import java.util.ArrayList;

public class ArrayProblems {
	
	String[] names = new String[100];
	int[] ages = new int[100];
	ArrayList myRawArray = new ArrayList();
	ArrayList<String> myBetterArray = new ArrayList<>();
	public static void main(String[] args) {
		
		ArrayProblems ap = new ArrayProblems();
		
		Object target;
		target = new String("name");
		if (target instanceof String) {
			ap.myRawArray.add(target);
		}
		target = 7;
		if (target instanceof String) {
			ap.myRawArray.add(target);
		}
		target = ap.myRawArray.get(0);
		if (target instanceof String) {
			String resultFromArray = (String) target;
			System.out.println(resultFromArray);
		}
		
	}

}
