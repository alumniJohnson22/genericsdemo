package com.m3.training.genericsdemo;

public class Driver {

	public static void main(String[] args) {
		Container<String> nameContainer = new Container<>();
		Container<Integer> intContainer = new Container<>();
		Container<Driver> driverContainer = new Container<>();

/*		nameContainer = GenericMethodDemo.getContainer("Java Class");
		System.out.println(nameContainer.remove());
		intContainer = GenericMethodDemo.getContainer(9);
		System.out.println(intContainer.remove());*/
		
		Container<Tiger> tigerCage = 
				GenericMethodDemo.getContainer(new Tiger());
		System.out.println(tigerCage.remove());
		Container<Feline> felineCage = 
				GenericMethodDemo.getContainer(new Feline());	
		System.out.println(felineCage.remove());	
		felineCage = GenericMethodDemo.getContainer(new Tiger());
		System.out.println(felineCage.remove());
		felineCage = GenericMethodDemo.getContainer(new Tiger());
		Feline target = felineCage.remove();
		if (target instanceof Tiger) {
			Tiger tiger = (Tiger) felineCage.remove();
			System.out.println(tiger);
		}
		
		Container<Feline> narrowFarm;
		narrowFarm = new Container<Feline>();
		//narrowFarm = new Container<Tiger>();
		
		Container<? extends Feline> farm;
		farm = new Container<Tiger>();	
		farm = new Container<Cat>();
		
		Feline someFeline = farm.remove();
		// Cat cat = farm.remove();

		Container<? super Feline> broadFarm;
		broadFarm = new Container<Feline>();
		Container<Object> objContainer = new Container<Object>();
		objContainer.add(new Tiger());
		objContainer.add("This is not a tiger, this is a string.");
		broadFarm = objContainer;
		//Feline feline = broadFarm.remove();
		Object object = broadFarm.remove();
	}
	

}
