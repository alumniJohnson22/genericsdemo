package com.m3.training.genericsdemo;

public class Container <E> {

	private E element;

	public E add(E element) {
		this.element = element;
		return element;
	}
	
	public E remove() {
		E element = this.element;
		this.element = null;
		return element;
	}
}
