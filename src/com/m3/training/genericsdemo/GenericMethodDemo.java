package com.m3.training.genericsdemo;

public class GenericMethodDemo {
	
	public static <E> Container<E> getContainer(E element) {
		Container<E> container = new Container<>();
		container.add(element);
		return container;
	}

}
